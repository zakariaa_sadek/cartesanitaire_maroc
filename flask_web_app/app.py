from flask import abort, Flask, jsonify, render_template, request
from flask_cors import CORS
from flask_mysqldb import MySQL
import os


app = Flask(__name__)
app.config["MYSQL_HOST"] = "db"
app.config["MYSQL_DB"]= os.environ['MYSQL_DATABASE']
app.config["MYSQL_USER"] = os.environ['MYSQL_USER']
app.config["MYSQL_PASSWORD"] = os.environ['MYSQL_PASSWORD']

# connection = mysql.connector.connect(host='db',database='my_db',user='root',password='helloworld')

mysql = MySQL(app)
CORS(app)


@app.route("/")
def index():
    # GET all data from database
    if request.method == "GET":
        cursor = mysql.connection.cursor()
        cursor.execute("SELECT * FROM regions")
        regions = cursor.fetchall()
        allData = []

        for i in range(len(regions)):
            id_de_region = regions[i][0]
            nom_de_region = regions[i][1]
            nombre_de_hopitaux = regions[i][2]
            nombre_lits_existants = regions[i][3]
            nombre_lits_fonctionnels = regions[i][4]
            nombre_centre_hemodialyse = regions[i][5]
            nombre_lit_urgence = regions[i][6]
            nombre_clinique = regions[i][7]
            nombre_de_pharmacies = regions[i][8]
            nombre_de_cabinet_labo = regions[i][9]
            nombre_med_general = regions[i][10]
            nombre_med_special = regions[i][11]
            nombre_med_dentist = regions[i][12]
            nombre_pharmaciens = regions[i][13]
            nombre_para = regions[i][14]
            dataDict = {
                "id_de_region": id_de_region,
                "nom_de_region": nom_de_region,
                "nombre_de_hopitaux": nombre_de_hopitaux,
                "nombre_lits_existants": nombre_lits_existants,
                "nombre_lits_fonctionnels": nombre_lits_fonctionnels,
                "nombre_centre_hemodialyse": nombre_centre_hemodialyse,
                "nombre_lit_urgence": nombre_lit_urgence,
                "nombre_clinique": nombre_clinique,
                "nombre_de_pharmacies": nombre_de_pharmacies,
                "nombre_de_cabinet_labo": nombre_de_cabinet_labo,
                "nombre_med_general": nombre_med_general,
                "nombre_med_special": nombre_med_special,
                "nombre_med_dentist": nombre_med_dentist,
                "nombre_pharmaciens": nombre_pharmaciens,
                "nombre_para": nombre_para
            }
            allData.append(dataDict)

        # return jsonify(allData)
        return render_template("home.html", product=allData)


@app.route("/regions")
def users():
    cursor = mysql.connection.cursor()
    cursor.execute("SELECT * FROM regions")
    regions = cursor.fetchall()
    allData = []
    for i in range(len(regions)):
        id_de_region = regions[i][0]
        nom_de_region = regions[i][1]
        nombre_de_hopitaux = regions[i][2]
        nombre_lits_existants = regions[i][3]
        nombre_lits_fonctionnels = regions[i][4]
        nombre_centre_hemodialyse = regions[i][5]
        nombre_lit_urgence = regions[i][6]
        nombre_clinique = regions[i][7]
        nombre_de_pharmacies = regions[i][8]
        nombre_de_cabinet_labo = regions[i][9]
        nombre_med_general = regions[i][10]
        nombre_med_special = regions[i][11]
        nombre_med_dentist = regions[i][12]
        nombre_pharmaciens = regions[i][13]
        nombre_para = regions[i][14]
        dataDict = {
            "id_de_region": id_de_region,
            "nom_de_region": nom_de_region,
            "nombre_de_hopitaux": nombre_de_hopitaux,
            "nombre_lits_existants": nombre_lits_existants,
            "nombre_lits_fonctionnels": nombre_lits_fonctionnels,
            "nombre_centre_hemodialyse": nombre_centre_hemodialyse,
            "nombre_lit_urgence": nombre_lit_urgence,
            "nombre_clinique": nombre_clinique,
            "nombre_de_pharmacies": nombre_de_pharmacies,
            "nombre_de_cabinet_labo": nombre_de_cabinet_labo,
            "nombre_med_general": nombre_med_general,
            "nombre_med_special": nombre_med_special,
            "nombre_med_dentist": nombre_med_dentist,
            "nombre_pharmaciens": nombre_pharmaciens,
            "nombre_para": nombre_para
        }
        allData.append(dataDict)

    return jsonify(allData)    


# @app.route('/regions/<string:id>', methods=['GET', 'DELETE', 'PUT'])
# def onedata(id):

#     # GET a specific data by id
#     if request.method == 'GET':
#         cursor = mysql.connection.cursor()
#         cursor.execute('SELECT * FROM users WHERE id = %s', (id))
#         users = cursor.fetchall()
#         print(users)
#         data = []
#         for i in range(len(users)):
#             id = users[i][0]
#             name = users[i][1]
#             age = users[i][2]
#             dataDict = {
#                 "id": id,
#                 "name": name,
#                 "age": age
#             }
#             data.append(dataDict)
#         return jsonify(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9090)
