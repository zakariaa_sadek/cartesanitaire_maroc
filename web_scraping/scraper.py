import requests
from bs4 import BeautifulSoup
import pandas as pd
import mysql.connector
import os

mysql_database = os.environ['MYSQL_DATABASE']
mysql_user = os.environ['MYSQL_USER']
mysql_password = os.environ['MYSQL_PASSWORD']

connection = mysql.connector.connect(host='db',database=mysql_database,user=mysql_user,password=mysql_password)
# connection = mysql.connector.connect(host='db',database='my_db',user='abcd',password='abcd')
cursor = connection.cursor()

r_id = []
r_name=[]
r_pharma=[]
r_hopitaux=[]
r_lit_existant=[]
r_lit_fonctionnel=[]
r_centre_hemodialyse=[]
r_lit_urgence=[]
r_cliniques=[]
r_cabinet_labo=[]

r_med_gen=[]
r_med_spe=[]
r_dentist=[]
r_pharmacien=[]
r_para=[]


les_regions_du_maroc = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
for g in les_regions_du_maroc:
    uri = 'http://cartesanitaire.sante.gov.ma/ftnrd?p_idniveau=4&p_idreg='+str(g)
    #print(uri)
    page_link = requests.get(uri)
    #print(page_link.status_code)
    parser = BeautifulSoup(page_link.content, "html.parser")
    # Id de region
    id_region = int(g)
    
    # Nom de region
    nom_de_region = parser.find('span',{ 'id' : 'lblRegion'}).get_text()
    nom_de_region = nom_de_region.replace('Région : ','')
    #print(nom_de_region)
    # Nombre d’hôpitaux
    nombre_de_hopitaux = parser.find_all("tbody")[4].find_all('tr')[8].find_all('td')[1].get_text()
    nombre_de_hopitaux  = int(nombre_de_hopitaux)
    #print(nombre_de_hopitaux)
    
    # Nombre de lits existants
    nombre_lits_existants =  parser.find_all("tbody")[4].find_all('tr')[8].find_all('td')[2].get_text()
    nombre_lits_existants  = int(nombre_lits_existants)
    # Nombre de lits fonctionnels
    nombre_lits_fonctionnels = parser.find_all("tbody")[4].find_all('tr')[8].find_all('td')[3].get_text()
    nombre_lits_fonctionnels  = int(nombre_lits_fonctionnels)
    
    # Nombre de centres d’hémodialyse
    nombre_centre_hemodialyse = parser.find_all("tbody")[5].find_all('tr')[0].find_all('td')[1].get_text()
    nombre_centre_hemodialyse  = int(nombre_centre_hemodialyse)
    
    # Nombre de lits d’urgence
    nombre_lit_urgence = parser.find_all("tbody")[9].find_all('tr')[3].find_all('td')[2].get_text()
    nombre_lit_urgence  = int(nombre_lit_urgence)

    # Nombre de cliniques
    nombre_clinique =  parser.find_all("tbody")[12].find_all('tr')[0].find_all('td')[1].get_text()
    nombre_clinique  = int(nombre_clinique)

    # Nombre d’officines de pharmacie
    nombre_de_pharmacies = parser.find_all("tbody")[12].find_all('tr')[4].find_all('td')[1].get_text()
    nombre_de_pharmacies  = int(nombre_de_pharmacies)

    # Nombre de cabinet de laboratoire
    nombre_de_cabinet_labo = parser.find_all("tbody")[12].find_all('tr')[3].find_all('td')[1].get_text()
    nombre_de_cabinet_labo = int(nombre_de_cabinet_labo)

    # Nombre de médecins généralistes
    nombre_med_general_public = parser.find_all("tbody")[14].find_all('tr')[0].find_all('td')[7].get_text()
    nombre_med_general_prive  = parser.find_all("tbody")[15].find_all('tr')[0].find_all('td')[1].get_text()
    nombre_med_general = int(nombre_med_general_public)+int(nombre_med_general_prive)

    # Nombre de médecins spécialistes
    nombre_med_special_public = parser.find_all("tbody")[14].find_all('tr')[1].find_all('td')[7].get_text()
    nombre_med_special_prive  = parser.find_all("tbody")[15].find_all('tr')[1].find_all('td')[1].get_text()
    nombre_med_special = int(nombre_med_special_public)+int(nombre_med_special_prive)

    # Nombre de chirurgiens-dentistes
    nombre_med_dentist = parser.find_all("tbody")[14].find_all('tr')[3].find_all('td')[7].get_text()
    nombre_med_dentist = int(nombre_med_dentist)

    # Nombre de pharmaciens
    nombre_pharmaciens = parser.find_all("tbody")[14].find_all('tr')[4].find_all('td')[7].get_text()
    nombre_pharmaciens = int(nombre_pharmaciens)

    # Nombre du corps paramédical (infirmiers, sages-femmes et similaires)
    nombre_para = parser.find_all("tbody")[14].find_all('tr')[9].find_all('td')[7].get_text()
    nombre_para = int(nombre_para)

    r_id.append(id_region)
    r_name.append(nom_de_region)
    r_hopitaux.append(nombre_de_hopitaux)
    r_lit_existant.append(nombre_lits_existants)
    r_lit_fonctionnel.append(nombre_lits_fonctionnels)
    r_centre_hemodialyse.append(nombre_centre_hemodialyse)
    r_lit_urgence.append(nombre_lit_urgence)
    r_cliniques.append(nombre_clinique)
    r_pharma.append(nombre_de_pharmacies)
    r_cabinet_labo.append(nombre_de_cabinet_labo)
    r_med_gen.append(nombre_med_general)
    r_med_spe.append(nombre_med_special)
    r_dentist.append(nombre_med_dentist)
    r_pharmacien.append(nombre_pharmaciens)
    r_para.append(nombre_para)

pdf=pd.DataFrame({
    'id_de_region':r_id,
    'nom_de_region':r_name, 
    'nombre_de_hopitaux': r_hopitaux, 
    'nombre_lits_existants' : r_lit_existant, 
    'nombre_lits_fonctionnels': r_lit_fonctionnel,
    'nombre_centre_hemodialyse': r_centre_hemodialyse,
    'nombre_lit_urgence' : r_lit_urgence,
    'nombre_clinique' : r_cliniques,
    'nombre_de_pharmacies': r_pharma,
    'nombre_de_cabinet_labo': r_cabinet_labo,
    'nombre_med_general' : r_med_gen,
    'nombre_med_special' : r_med_spe,
    'nombre_med_dentist' : r_dentist,
    'nombre_pharmaciens' : r_pharmacien,
    'nombre_para' : r_para
})
print("End Scraping")
# print(pdf)

cols = "`,`".join([str(i) for i in pdf.columns.tolist()])

# Insert DataFrame recrds one by one.
for i,row in pdf.iterrows():
    # Insert a new row only if data do not exist
    sql = "INSERT IGNORE INTO `regions` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
    cursor.execute(sql, tuple(row))

    # the connection is not autocommitted by default, so we must commit to save our changes
    connection.commit()
