# About the Application

Dockerized application that scrapes data from http://cartesanitaire.sante.gov.ma/ , turns it to a DataFrame and insert it to a SQL DataBase.
And then we can see the result in a web page ( as rest api ( GET ) , and as an html Table ) 

## Detailed description

This is a multi-container applications. With 4 components : 

- db : mysql databases to store the result of the scraped data.
---
**NOTE**

There is a mysql_init file that contains a sql script that initialize our database, I mean it creates the table and its columns

---
- phpmyadmin : *(Optional)* to handle the administration of MySQL over the UI.
- scraper : Python based application that runs the scripts responsible of scraping needed informations from [The official website of Moroccan Ministry of Health](http://cartesanitaire.sante.gov.ma/), collect the necessary informations of the twelve regions, generate a new DataFrame using **Pandas** and store the created DataFrame in the mysql database
- flask_web_app : **Flask** web application that has two routes : 
    1. root ('/') that displays data in a bootstraped table
    2. subPath ('/regions') that displays data in a JSON format (Get API)

## How to Run the application

- Simply run :
````
docker-compose up -d
````
- Wait until ***scraper*** service exits(which mean that the script worked as expected *(see logs to verify this)*)
- Use your internal IP address or localhost *if it's locally deployed of course :stuck_out_tongue_winking_eye:*