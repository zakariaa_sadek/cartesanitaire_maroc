DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions`(
    `id_de_region` int(5) NOT NULL ,
    `nom_de_region` varchar(255) NOT NULL, 
    `nombre_de_hopitaux` int(4) NOT NULL, 
    `nombre_lits_existants` int(4) NOT NULL, 
    `nombre_lits_fonctionnels` int(4) NOT NULL,
    `nombre_centre_hemodialyse` int(4) NOT NULL,
    `nombre_lit_urgence` int(4) NOT NULL,
    `nombre_clinique` int(4) NOT NULL,
    `nombre_de_pharmacies` int(4) NOT NULL,
    `nombre_de_cabinet_labo` int(4) NOT NULL,
    `nombre_med_general` int(4) NOT NULL,
    `nombre_med_special` int(4) NOT NULL,
    `nombre_med_dentist` int(4) NOT NULL,
    `nombre_pharmaciens` int(4) NOT NULL,
    `nombre_para` int(4) NOT NULL,
    PRIMARY KEY (`id_de_region`)
);